// Tipos de Datos

//String
"Hello World"
'Hello World'

//numéricos number
1000
345.32
-232

//Boolean o lógico
true
false

//array o arreglo
["Silvia","Catherine","Julia"]
[1,2,3]
[true,false,true]

//Object u objeto ("Clave":"Valor")
{
    "username": "Ryan",
    "score": 70.4,
    "hours": 14,
    "professional": true,
    "friends": ["Kesha","Allison","Cloe"]
}

//Consola (comunicación con la máquina)
console.log('Esto es una cadena');

//Varieble
var Username = "Jenny";
let Lastname = "Hernández"; 

Username: "PEPE";

console.log("Username")

//Constantes

const PI = 3.14159;
const Id = 309234155;
// PI = 100; (error)

/* Las variables no pueden iniciar con numero o @

1username = Sandra; (error)

Sí se puede utilizar $ y _
*/

//Camel-case

nombreDePersona = "Annie";

let numberOne = 60;
let numberTwo = 100;

let result = numberOne + numberTwo;

console.log = result;

//Concatenación (uniendo dos String)

let name = "Rose";
let surname = "Marie";

let completeName = name + " " + surname;

console.log (completeName);

//Comparaciones (<,>,!=,==,>=,<=)

let numberThree = 100;
let numberFour= 500;

let resultado = numberThree > numberFour;

console.log (resultado);

//Flujo de control condicionales (Todos los programas tienen control de flujo, que es como se comporta el programa dependiendo los resultados)

// If

if (resultado === true) {

    console.log ("Login Correcto");

} else {

    console.log ("Contraseña Incorrecta");

}

//If multiples variables.

let score = 70;

if (score > 30) {

    console.log ("Necesitas practicar más");

} else if (score > 60) {

    console.log ("Muy bien");

} else {

    console.log ("Ve este tutorial");

}

//Switch

let typeCard = "debito";

switch (typeCard) {

    case "debito":
        console.log ("Esta es una tarjeta de debito");
    break;

    case "credito":
        console.log ("Esta es una tarjeta de credito");
    break;

    default:
        console.log ("No posees una tarjeta");
}

//Bucles

let count = 50;

while (count > 0) {

    console.log ("Hello world");
    console.log (count);
    count = count - 1;

}


let names = ["Gina","Zule","Jess","Emily"];

console.log (names[0]);

console.log (names.length);

for (let i = 0; i < names.length; i++) {

    console.log (names[i]);

} 

//Funciones

function greeting(name) {

    console.log (name);

    console.log ("Hello");

}

greeting(Rachel);


function add(n1, n2) {

    console.log (n1, n2);

}

add(3, 2);












 




 


